package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import static android.view.View.generateViewId;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LessonsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LessonsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LessonsFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button fidel_button;
    private TableLayout tableLayout;
    private List<List<String>> character_list;

    private OnFragmentInteractionListener mListener;

    public LessonsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LessonsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LessonsFragment newInstance(String param1, String param2) {
        LessonsFragment fragment = new LessonsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    public void setTextViewText(String s)
    {
        fidel_button.setText(s);
        fidel_button.setTextSize(18);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_lessons, container, false);
        inflate.findViewById(R.id.fragment_lessons_character).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_lessons_next_button).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_lessons_prev_button).setOnClickListener(this);
        fidel_button = (Button) inflate.findViewById(R.id.fragment_lessons_character);
        tableLayout = (TableLayout) inflate.findViewById(R.id.fragment_lessons_table);
        initTable(tableLayout,character_list);

        return inflate;
    }

    public void setArguments(List<List<String>> character_list)
    {
        this.character_list = character_list;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onFidelSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {


        if ( (""+v.getTag()).equals("prev"))
        {
            mListener.onPrevPressed();
        }
        else if ((""+v.getTag()).equals("next"))
        {
            mListener.onNextPressed();
        }
        else
        {
            mListener.onCharPressed(""+v.getTag());
        }
        
    }


    private void initTable(TableLayout tableLayout, List<List<String>> fidel_list)
    {
//        String[] array_1 = {"ha","ሀ", "ሁ", "ሂ", "ሃ", "ሄ", "ህ", "ሆ"};
//        String[] array_2 = {"le","ለ", "ሉ", "ሊ", "ላ", "ሌ", "ል", "ሎ"};
//        String[] array_3 = {"ha","ሐ", "ሑ", "ሒ", "ሓ", "ሔ", "ሕ", "ሖ"};
//        String[] array_4 = {"se","ሠ", "ሡ", "ሢ", "ሣ", "ሤ", "ሥ", "ሦ"};
//        String[] array_5 = {"me","መ", "ሙ", "ሚ", "ማ", "ሜ", "ም", "ሞ"};
//        tableLayout = new TableLayout(getActivity().getApplicationContext());

        String[] array_bete = {"ግእዝ", "ካይብ","ሳልስ","ራብእ","አምስ","ሳድስ","ሳብእ"};
//        for(List<String> l : fidel_list)
//        {
//            for (String ll : l)
//            {
//                Log.d(" WHERE IS IT ", ll);
//            }
//        }
//        String[][] disp = new String[][] {array_1, array_2, array_3, array_5,array_4};
        Context applicationContext = getActivity().getApplicationContext();



        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        tableLayout.setLayoutParams(lp);
        tableLayout.setStretchAllColumns(true);

        TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1.0f);
        TableRow.LayoutParams cellLp = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1.0f);

        TableRow rHeader = new TableRow(applicationContext);

        for (String idx: array_bete)
        {
            TextView textView = new TextView(applicationContext);
            textView.setText(idx);
//            textView.setTextSize(16);
            Drawable drawable =  applicationContext.getDrawable(android.R.drawable.title_bar);
            textView.setBackground(drawable);

            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//            textView.setPadding(8,8,8,8);

            rHeader.addView(textView,cellLp);
        }
        tableLayout.addView(rHeader,rowLp);
        Log.d("Fidel is empty ", String.valueOf(fidel_list == null));
        int index =0;
        assert fidel_list != null;
        for(List<String> list : fidel_list)
        {

            if (applicationContext != null) {
                TableRow r = new TableRow(applicationContext);
                for(int i = 1; i < list.size(); i++) {
                    Button textView = new Button(applicationContext);
                    textView.setText(list.get(i));
//                    textView.setTextSize(14);
                    int i1 = generateViewId();
                    textView.setId(i1);

                    //Here we add the unicode amharic to identify the index.
                    textView.setTag(list.get(1) + " " + i + " " + index);

                    Drawable drawable =  applicationContext.getDrawable(android.R.drawable.btn_default_small);
                    textView.setBackground(drawable);

//                    textView.setPadding(5,5,5,5);
                    textView.setOnClickListener(this);
                    r.addView(textView,cellLp);

                }
            index++;
                tableLayout.addView(r,rowLp);
            }
        }
        //TODO fill the table
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onNextPressed();
        void onPrevPressed();
        void onCharPressed(String charPressed);
    }
}
