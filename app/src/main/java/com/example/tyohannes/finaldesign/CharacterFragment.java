package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.generateViewId;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link onFidelSelected} interface
 * to handle interaction events.
 * Use the {@link CharacterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CharacterFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ArrayList<? extends List<String>> fidel_list;
    private onFidelSelected mListener;

    public CharacterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CharacterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CharacterFragment newInstance(String param1, String param2) {
        CharacterFragment fragment = new CharacterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_character_list, container, false);
        TableLayout layout = (TableLayout) inflate.findViewById(R.id.fragment_character_list_table);
        initTable(layout);

        if (savedInstanceState != null)
        {
            fidel_list = savedInstanceState.getParcelableArrayList("x");
        }

        return inflate;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String string) {
        if (mListener != null) {
            mListener.onFidelInteraction(string);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onFidelSelected) {
            mListener = (onFidelSelected) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onFidelSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        mListener.onFidelInteraction("" + v.getTag());

    }


    @Override
    public void onSaveInstanceState(Bundle outstate)
    {
        super.onSaveInstanceState(outstate);
//        outstate.putSerializable("array",fidel_list);
        outstate.putParcelableArrayList("x", (ArrayList<? extends Parcelable>) fidel_list);

    }

    public void setArguments(List<List<String>> fidel_list) {
     this.fidel_list = (ArrayList<? extends List<String>>) fidel_list;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface onFidelSelected {
        // TODO: Update argument type and name
        void onFidelInteraction(String id_clicked);
    }

    private void initTable(TableLayout layout)
    {
//        String[] array_1 = {"ha","ሀ", "ሁ", "ሂ", "ሃ", "ሄ", "ህ", "ሆ"};
//        String[] array_2 = {"le","ለ", "ሉ", "ሊ", "ላ", "ሌ", "ል", "ሎ"};
//        String[] array_3 = {"ha","ሐ", "ሑ", "ሒ", "ሓ", "ሔ", "ሕ", "ሖ"};
//        String[] array_4 = {"se","ሠ", "ሡ", "ሢ", "ሣ", "ሤ", "ሥ", "ሦ"};
//        String[] array_5 = {"me","መ", "ሙ", "ሚ", "ማ", "ሜ", "ም", "ሞ"};
        String[] array_bete = {"ግእዝ", "ካይብ","ሳልስ","ራብእ","አምስ","ሳድስ","ሳብእ"};
//        for(List<String> l : fidel_list)
//        {
//            for (String ll : l)
//            {
//                Log.d(" WHERE IS IT ", ll);
//            }
//        }
//        String[][] disp = new String[][] {array_1, array_2, array_3, array_5,array_4};
        Context applicationContext = getActivity().getApplicationContext();

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(lp);
        layout.setStretchAllColumns(true);

        TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1.0f);
        TableRow.LayoutParams cellLp = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1.0f);

        TableRow rHeader = new TableRow(applicationContext);

        for (String idx: array_bete)
        {
            TextView textView = new TextView(applicationContext);
            textView.setText(idx);
            textView.setTextSize(16);
            Drawable drawable =  applicationContext.getDrawable(R.drawable.cell_shape);
            textView.setBackground(drawable);

//            textView.setPadding(8,8,8,8);

            rHeader.addView(textView,cellLp);
        }
        layout.addView(rHeader,rowLp);
        Log.d("Fidel is empty ", String.valueOf(fidel_list == null));
        for(List<String> list : fidel_list)
        {

            if (applicationContext != null) {


                TableRow r = new TableRow(applicationContext);

                for(int i = 1; i < list.size(); i++) {

                    Button textView = new Button(applicationContext);
                    textView.setText(list.get(i));
                    textView.setTextSize(14);
                    int i1 = generateViewId();
                    textView.setId(i1);
                    textView.setTag(list.get(1) + " " + i);

                    Drawable drawable =  applicationContext.getDrawable(R.drawable.cell_shape);
                    textView.setBackground(drawable);

                    textView.setPadding(5,5,5,5);
                    textView.setOnClickListener(this);
                    r.addView(textView,cellLp);

                }

                layout.addView(r,rowLp);
            }
        }
        //TODO fill the table
    }
}
