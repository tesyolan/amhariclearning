package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


/**
 Okay; This is the main QuizFragment; It's related view is detailed in @fragment_quiz.xml in layout
 folder. Basically we get the buttons and textviews on the onCreateView method and assign it to
 choice_1, choice_2, choice_3, choice_4 and next button attributes.

 As this is a fragment we do have an interface for any activity that is bound to it should hold
 that is defined in the OnFragmentInteractionListener; This interface is essentially overriden in the
 MainActivity as it implements QuizFragments.OnFrag..Listerner. So this would be set
 */
public class QuizFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button playbutton;
    private Button choice_1;
    private Button choice_2;
    private Button choice_3;
    private Button choice_4;
    private Button next;

    private TextView time_remaining;
    private TextView question_remaining;
    private TextView score;
    private TextView information;

    private CountDownTimer countDownTimer;
    private OnFragmentInteractionListener mListener;
    private List<List<String>> character_list;

    private QuizManager quizManager;
    public QuizFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QuizFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuizFragment newInstance(String param1, String param2) {
        QuizFragment fragment = new QuizFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_quiz, container, false);

        inflate.findViewById(R.id.fragment_quiz_button3).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_quiz_button4).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_quiz_button5).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_quiz_button6).setOnClickListener(this);
        inflate.findViewById(R.id.fragment_quiz_question_button).setOnClickListener(this);

        playbutton = (Button) inflate.findViewById(R.id.fragment_quiz_question_button);
        choice_1 = (Button) inflate.findViewById(R.id.fragment_quiz_button3);
        choice_2 = (Button) inflate.findViewById(R.id.fragment_quiz_button4);
        choice_3= (Button) inflate.findViewById(R.id.fragment_quiz_button5);
        choice_4= (Button) inflate.findViewById(R.id.fragment_quiz_button6);
        next= (Button) inflate.findViewById(R.id.fragment_quiz_next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setQuestion();
            }
        });

        next.setVisibility(View.INVISIBLE);

        time_remaining = (TextView) inflate.findViewById(R.id.fragment_quiz_text_time);
        question_remaining= (TextView) inflate.findViewById(R.id.fragment_quiz_text_remaining_q);
        score = (TextView) inflate.findViewById(R.id.fragment_quiz_text_score);
        information = (TextView) inflate.findViewById(R.id.fragment_quiz_finish);
        quizManager = new QuizManager(character_list);

        setQuestion();
        return inflate;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        mListener.onChoice(Integer.parseInt(v.getTag()+""));
    }

    public void setArguments(List<List<String>> tobelearned_keys) {
        this.character_list = tobelearned_keys;
    }

    public int answer()
    {
        return quizManager.getAnswer_q();
    }
    public boolean checkQuestion(int choice)
    {
        int answer_q = quizManager.getAnswer_q();
        Log.d("Answer is", String.valueOf(choice));
        Log.d("Correct Answer is", String.valueOf(answer_q));
        if ((choice-1)==answer_q)
        {
            Log.d("Got Answer Correctly","");
            return true;
        }
        else
        {
            switch (choice)
            {

                case 1:
                    choice_1.setBackgroundColor(Color.RED);
                    break;
                case 2:
                    choice_2.setBackgroundColor(Color.RED);
                    break;
                case 3:
                    choice_3.setBackgroundColor(Color.RED);
                    break;
                case 4:
                    choice_4.setBackgroundColor(Color.RED);
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    public void setQuestion()
    {
        if(countDownTimer != null)
            countDownTimer.cancel();
        List<String> strings = quizManager.generateQuestion();

        choice_1.setText(strings.get(0));
        choice_2.setText(strings.get(1));
        choice_3.setText(strings.get(2));
        choice_4.setText(strings.get(3));

        choice_1.setBackgroundResource(android.R.drawable.btn_default);
        choice_2.setBackgroundResource(android.R.drawable.btn_default);
        choice_3.setBackgroundResource(android.R.drawable.btn_default);
        choice_4.setBackgroundResource(android.R.drawable.btn_default);

        choice_1.setTextColor(Color.BLACK);
        choice_2.setTextColor(Color.BLACK);
        choice_3.setTextColor(Color.BLACK);
        choice_4.setTextColor(Color.BLACK);




        next.setVisibility(View.INVISIBLE);

        countDownTimer = new CountDownTimer(10100, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time_remaining.setText(String.format(":%02d", Integer.parseInt(String.valueOf(millisUntilFinished / 1000))));
                time_remaining.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }

            @Override
            public void onFinish() {
                time_remaining.setText(String.format(":%02d", 0));
                next.setVisibility(View.VISIBLE);

                int answer = answer();
                switch (answer)
                {
                    case 0:
                        choice_1.setBackgroundColor(Color.BLUE);
                        choice_1.setTextColor(Color.WHITE);
                        break;
                    case 1:
                        choice_2.setBackgroundColor(Color.BLUE);
                        choice_2.setTextColor(Color.WHITE);
                        break;
                    case 2:
                        choice_3.setBackgroundColor(Color.BLUE);
                        choice_3.setTextColor(Color.WHITE);
                        break;
                    case 3:
                        choice_4.setBackgroundColor(Color.BLUE);
                        choice_4.setTextColor(Color.WHITE);
                        break;

                }

                time_remaining.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }.start();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onChoice(int id);
    }
}
