package com.example.tyohannes.finaldesign;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class NewUserActivity extends AppCompatActivity {
    ListView listview;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private Button createButton;
    private ImageListAdapter imageListAdapter;
    private List<Integer> array_images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);


        array_images = new ArrayList<>();

        array_images.add(R.drawable.boy_64);
        array_images.add(R.drawable.girl_64);
        array_images.add(R.drawable.male_64);
        array_images.add(R.drawable.female_64);


        recyclerView = (RecyclerView) findViewById(R.id.activity_new_user_list);

        layoutManager = new LinearLayoutManager(this);

        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);


        recyclerView.setLayoutManager(layoutManager);

//        listview = (ListView)findViewById(R.id.activity_new_user_list);
        imageListAdapter = new ImageListAdapter(this, array_images);
        recyclerView.setAdapter(imageListAdapter);

        createButton = (Button) findViewById(R.id.activity_new_user_create);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check if the user entered a good value meaning the values in the text are not empty.
                int avatar_index = imageListAdapter.getSelected_index();
                String user_name;
                user_name = (String) ((EditText)findViewById(R.id.activity_new_user_id)).getText().toString();
                UserDAO userDAO = new UserDAO(NewUserActivity.this);
                userDAO.insert(new User(user_name,0, avatar_index,0));
                userDAO.close();
                finish();
            }
        });


//        recyclerView.setAdapter();

    }


}
