package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.util.Log;
import android.view.CollapsibleActionView;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by tyohannes on from the website:
 * https://examples.javacodegeeks.com/android/core/graphics/canvas-graphics/android-canvas-example/
 */

public class CanvasView extends View {


    private String canvasText;
    private String canvasSave;
    public int width;
    public int height;
    private Bitmap mBitmap;
    private Bitmap hBitmap;
    private Canvas mCanvas;
    private Canvas hCanvas;
    private int count;
    private Path mPath;
    Context context;
    private Paint mPaint;
    private Paint _mPaint;
    private float mX, mY;
    private static final float TOLERANCE = 5;
    private File mFile;


    public CanvasView(Context c, AttributeSet attrs) {
        super(c, attrs);
        context = c;
        count = 0 ;
        // we set a new Path
        mPath = new Path();

        setDrawingCacheEnabled(true);

        // and we set a new Paint with the desired attributes
        mPaint = new Paint();

        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(4f);
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f  = new ColorMatrixColorFilter(cm);
        mPaint.setColorFilter(f);

        _mPaint = new Paint();
        _mPaint.setTextSize(200);
        _mPaint.setAntiAlias(true);
        _mPaint.setColor(Color.BLACK);
        _mPaint.setColorFilter(f);
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // your Canvas will draw onto the defined Bitmap
        height = h;
        width = w;

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mBitmap.eraseColor(Color.BLACK);
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(Color.BLACK);
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the mPath with the mPaint on the canvas when onDraw
        if(canvasText == null)
            canvasText = "";

        canvas.drawText(canvasText,width/2-width/6,height/2,_mPaint);
        canvas.drawPath(mPath, mPaint);
    }

    public File saveCanvas()
    {
        invalidate();
        FileOutputStream out = null;

        mBitmap = getDrawingCache();

        RectF bounds = new RectF();

        mPath.computeBounds(bounds,true);

        Rect pathBounds = new Rect();
        bounds.roundOut(pathBounds);

        if(pathBounds.width() == 0 || pathBounds.height() == 0)
            return null;

        hBitmap = Bitmap.createBitmap(pathBounds.width(), pathBounds.height()
                    , Bitmap.Config.ARGB_8888);
        hCanvas = new Canvas(hBitmap);

        hCanvas.translate(-pathBounds.left,-pathBounds.top);
        hCanvas.drawPath(mPath,mPaint);

        try {
            File file = new File(Environment.getExternalStorageDirectory(),"/Canvas/" + this.canvasSave + "/" );

            if(!file.exists())
            {
                boolean mkdirs = file.mkdirs();
                if(!mkdirs)
                {
                    Log.e("Failed to Create","Directories");
                }
            }

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            File file1 = new File(file + "/" + this.canvasSave + String.valueOf(count) + ".jpg");
            Log.d("canvas text",file.getAbsolutePath());
            while (file1.exists())
            {
                count++;
                file1 = new File(file + "/" + this.canvasSave + String.valueOf(count) + ".jpg");
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file1);
            hBitmap = getDrawingCache();
            hBitmap.compress(Bitmap.CompressFormat.JPEG,90,fileOutputStream);

            //Reconstruct the image;
            fileOutputStream.flush();
            fileOutputStream.close();
            clearCanvas();
            invalidate();
            count++;
            return file1;
        } catch (IOException e) {

            Log.e("Error", String.valueOf(e));
        }
        return null;

    }

    // when ACTION_DOWN start touch according to the x,y values
    private void startTouch(float x, float y) {
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    // when ACTION_MOVE move touch according to the x,y values
    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    public void clearCanvas() {
        mPath.reset();
        invalidate();
    }

    // when ACTION_UP stop touch
    private void upTouch() {
        mPath.lineTo(mX, mY);
    }

    public File returnFile()
    {
        return mFile;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    public void setCanvasText(String canvasText) {
        this.canvasText = canvasText;
        clearCanvas();
    }

    public void setSaveFolder(String canvasText) {
        Log.d("The save location",canvasText);
        this.canvasSave = canvasText;
    }


    public Bitmap getBitmap() {
        return hBitmap;
    }
}
