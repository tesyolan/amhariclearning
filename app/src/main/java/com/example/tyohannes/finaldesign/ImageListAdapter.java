package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by tyohannes on 1/9/17.
 */

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    private List<Integer> items;
    private Context context;
    private int selected_index;
    public ImageListAdapter(Context context,List<Integer> items) {
        this.context = context;
        this.items= items;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new ViewHolder(view, new ViewHolder.IMyViewHolderClicks(){
            public void onClick_(ImageView view1) {
                setSelected_index((int) view1.getTag(R.id.activity_new_user_list));
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageView im = (ImageView) holder.img_android;
        im.setTag(R.id.activity_new_user_list,position);
        Picasso.with(context)
                .load(items.get(position))
                .into(im);
//        Picasso.with(context).load(android_versions.get(i).getAndroid_image_url()).resize(120, 60).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getSelected_index() {
        return selected_index;
    }

    public void setSelected_index(int selected_index) {
        this.selected_index = selected_index;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tv_android;
        ImageView img_android;
        IMyViewHolderClicks mListener;
        public ViewHolder(View view, IMyViewHolderClicks vegetables) {
            super(view);

//            img_list = (TextView)view.findViewById(R.id.tv_android);
            mListener = vegetables;
            img_android = (ImageView)view.findViewById(R.id.list_item_image);
            img_android.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ImageView im= (ImageView)v;
            mListener.onClick_(im);
        }
        public static interface IMyViewHolderClicks{
            public void onClick_(ImageView view);
        }
    }
//    List<Integer> items;
//    public ImageListAdapter(Context context, int resource, List<Integer> items) {
//        super(context, resource, items);
//        this.items = items;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (convertView == null) {
//            LayoutInflater inflater = LayoutInflater.from(getContext());
//            convertView = inflater.inflate(R.layout.list_item, null);
//        }
//
//        Picasso.with(getContext())
//                .load(items.get(position))
//                .into((ImageView) convertView);
//
//        return convertView;
//    }
}

