package com.example.tyohannes.finaldesign;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AudioRecordFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AudioRecordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AudioRecordFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final int REQUEST_AUDIO_PERMISSION = 200;
    private static String mFilename = null;

    private Button mRecordButton = null;
    private Button mNext = null;
    private Button mPrevious = null;
    private TextView mTextView = null;
    private MediaRecorder mRecord = null;

    private boolean permissionToRecordAccepted = false;

    private String [] permission = {Manifest.permission.RECORD_AUDIO};

    private OnFragmentInteractionListener mListener;



    public AudioRecordFragment() {
        // Required empty public constructor
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) getActivity().finish();
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AudioRecordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AudioRecordFragment newInstance(String param1, String param2) {
        AudioRecordFragment fragment = new AudioRecordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_record_audio, container, false);
        mRecordButton = (Button)inflate.findViewById(R.id.fragment_record_audio_record);
        mNext = (Button)inflate.findViewById(R.id.fragment_record_audio_next);
        mPrevious = (Button)inflate.findViewById(R.id.fragment_record_audio_previous);
        mTextView = (TextView)inflate.findViewById(R.id.fragment_record_audio_text);
        setTextView(mParam1);
        mRecordButton.setOnClickListener(this);
        mNext.setOnClickListener(this);
        mPrevious.setOnClickListener(this);

        return inflate;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void startRecording() {
        mRecord = new MediaRecorder();
        mRecord.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecord.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        //TODO check the file name is not null;
        mRecord.setOutputFile(mFilename);
        mRecord.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecord.prepare();
        }
        catch (IOException e) {
            Log.e("Error", "prepare() failed");
        }

        mRecord.start();
    }

    private void stopRecording() {
        mRecord.stop();
        mRecord.release();
        mRecord = null;
    }

    private void getFileName() {
        //TODO this would return the filename to actually find the file using Media Sync
    }

    public void setTextView(String string) {
        //TODO the text to actually set the system.
        mTextView.setTextSize(24);
        int count = 0;

            File file = new File(Environment.getExternalStorageDirectory(), "/Canvas_AUDIO/" + string + "/");

            if (!file.exists()) {
                boolean mkdirs = file.mkdirs();
                if (!mkdirs) {
                    Log.e("Failed to Create", "Directories");
                }
            }

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            File file1 = new File(file + "/" + string + String.valueOf(count) + ".3gp");
            Log.d("canvas audio", file.getAbsolutePath());
            while (file1.exists()) {
                count++;
                file1 = new File(file + "/" + string + String.valueOf(count) + ".3gp");
            }
//            FileOutputStream fileOutputStream = new FileOutputStream(file1);
            mFilename = file1.toString();
        mTextView.setText(string);
        Log.d("File name", mFilename);

    }

    @Override
    public void onClick(View v) {


        if (v.getTag().toString().equals("ቅርጽ"))
        {
            Log.d("Get's","rec");
            //TODO do record stuff here
            //INSERT TIME HERE it would make it easier.
            startRecording();
            new CountDownTimer(3100, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    Log.d("Recording: ", mFilename);
                    mRecordButton.setText("Stop");
                }

                @Override
                public void onFinish() {
                    stopRecording();
                    Log.d("Finished: ", "Finished");
                    MediaScannerConnection.scanFile(getActivity().getApplicationContext(),
                            new String[]{mFilename}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                }
                            });
                    mRecordButton.setText("ቅርጽ");
                    //TODO this is to make sure that i create a new file to save to next time around.
                    setTextView(mTextView.getText().toString());
                }
            }.start();
        }
        else if (v.getTag().toString().equals("ቀጣይ"))
        {
            Log.d("Get's","rec");
            //TODO do next stuff change the file. if record is being done stop that here
            mListener.OnFragmentInteractionListenerAudio(true);
        }
        else if (v.getTag().toString().equals("ያለፈ"))
        {
            Log.d("Get's","rec");
            //TODO same as next as above but for previous
            mListener.OnFragmentInteractionListenerAudio(false);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnFragmentInteractionListenerAudio(boolean bool);
    }
}
