package com.example.tyohannes.finaldesign;

import java.io.Serializable;

/**
 * This class is used to handle the following states:
 * The user id
 * The user avatar id
 * The user progress
 * Created by tyohannes on 1/9/17.
 */

public class User implements Serializable {
    private int id; //this is obtained from the db
    private int avatar_id;
    private String user_name;
    private int progress_level;

    public User(String user_name, int id, int avatar_id, int progress_level)
    {
        this.user_name = user_name;
        this.id = id;
        this.avatar_id = avatar_id;
        this.progress_level = progress_level;
    }

    public User() {
        this.user_name = "";
        this.id = 0;
        this.avatar_id = 0;
        this.progress_level = 0;
    }

    public int getProgressLevel()
    {
        return progress_level;
    }
    //This increase the level to the next integer
    public void increaseLevel()
    {
        this.progress_level++;
    }

    public String getName() {
        return user_name;
    }
    public int getAvatar() {
        return avatar_id;
    }

    @Override
    public String toString(){
        return getName();
    }

}
