package com.example.tyohannes.finaldesign;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by tyohannes on 1/19/17.
 */

public class QuizManager {
    private List<String> askedQuestion;
    private List<String> listOfQuestions;
    private List<String> unique_ids;

    private int answer_q;
    public QuizManager(List<List<String>> listOfQuestions)
    {
        this.askedQuestion = new ArrayList<String>();
        this.listOfQuestions = new ArrayList<String>();
        this.unique_ids = new ArrayList<String>();
        for(List<String> l:listOfQuestions)
        {
            for(int i=1; i<l.size(); i++)
            {

                this.listOfQuestions.add(l.get(i));
            }

            //use this for later to generate unique ids.
            unique_ids.add(l.get(0));
        }

    }

    public List<String> generateQuestion()
    {
        List<String> new_question = null;

        Random new_q_index = new Random();


        boolean asked_question=false;

        String new_question_id = null;
            while(true) {
                int q_index = new_q_index.nextInt(listOfQuestions.size());
                new_question_id = (String) (listOfQuestions.get(q_index));
                for (String q : askedQuestion)
                {
                    if (q.equals(new_question_id))
                    {
                        asked_question= true;
                    }
                }
                if(!asked_question)
                {
                    break;
                }
            }

        //Now we return resource id; three incorrect answers and one correct answer.
        new_question = new ArrayList<String>();

        int answer_index = new_q_index.nextInt(4);
        answer_q = answer_index;
//        String[] question = new String[4];

//        question[answer_index] = new_question_id;
        List<String> non_answers = returnNonAnswers(new_question_id);
        Iterator<String> iterator = non_answers.iterator();
        for(int i=0; i < 4
                ; i++)
        {
            if(i!=answer_index)
            {
                new_question.add(i, iterator.next());
            }
            else
            {
                new_question.add(i,new_question_id);
            }
        }


        return new_question;
    }

    public int getAnswer_q() {
        return answer_q;
    }

    private List<String> returnNonAnswers(String answer)
    {
        //TODO we can make this a variable array.
        List<String> nonanswers= new ArrayList<String>();


        for(int i=0; i<3;i++)
        {
            while(true) {
                Random random = new Random();
                int rand = random.nextInt(listOfQuestions.size());
                String nonanswer = (String)listOfQuestions.get(rand);
                if (!answer.equals(nonanswer) && !nonanswers.contains(nonanswer)) {
                    nonanswers.add(i, nonanswer);
                    break;
                }
            }
        }
        return nonanswers;
    }

}
