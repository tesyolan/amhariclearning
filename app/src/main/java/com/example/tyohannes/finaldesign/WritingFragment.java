package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WritingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WritingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WritingFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    CanvasView canvasView;
    CanvasView canvasView_text;
    Button button_clear;

    private Button next_button;
    private Button previous_button;
    private Button save_button;

    private TextView textToWrite;

//    private ImageView reloadBitmap;

    private OnFragmentInteractionListener mListener;

    public WritingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WritingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WritingFragment newInstance(String param1, String param2) {
        WritingFragment fragment = new WritingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_writing, container, false);
        canvasView = (CanvasView)inflate.findViewById(R.id.fragment_writing_canvas);
        canvasView_text = (CanvasView)inflate.findViewById(R.id.fragment_writing_target);
//        textToWrite = (TextView) inflate.findViewById(R.id.fragment_writing_write_this);
//        textToWrite.setTextSize(100);
        canvasView.setSaveFolder(mParam1);
        canvasView_text.setCanvasText(mParam1);
//        textToWrite.setText(mParam1);
        button_clear = (Button)inflate.findViewById(R.id.fragment_writing_clear);
        button_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.clearCanvas();
            }
        });

        next_button = (Button)inflate.findViewById(R.id.fragment_writing_next);
        next_button.setOnClickListener(this);
        previous_button = (Button)inflate.findViewById(R.id.fragment_writing_previous);
        previous_button.setOnClickListener(this);
        save_button = (Button)inflate.findViewById(R.id.fragment_writing_save);
        save_button.setOnClickListener(this);

//        reloadBitmap = (ImageView) inflate.findViewById(R.id.fragment_writing_reload);



        return inflate;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (((String)v.getTag()).equals("1"))
        {
            Log.d("Going: ","Forward");
            mListener.onFragmentInteraction(true);
        }
        else if (((String)v.getTag()).equals("0"))
        {
            Log.d("Going: ","Back");
            mListener.onFragmentInteraction(false);
        }
        else if (((String)v.getTag()).equals("Save"))
        {
            //Trigger the save sequence; Now How i am going to collect the data;
            // -> I would save For Each Sample Numerous Writings I collect;
                // -> So It must save names in unique combination everytime. It collects data using
                // -> folder for each sample.
                // -> I must not save data that is not an accurate representation of the system.
                // -> Now When I reenter the application; I must be able to add to the existing data.
            // -> Then Train and See the output.
            File file = canvasView.saveCanvas();
            Bitmap bitmap = canvasView.getBitmap();
            if (file != null) {
                MediaScannerConnection.scanFile(getActivity().getApplicationContext(),
                        new String[]{file.toString()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });
                BitmapDrawable imagePath = new BitmapDrawable(getResources(), file.toString());

//                reloadBitmap.setImageBitmap(imagePath.getBitmap());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] imageBytes = byteArrayOutputStream.toByteArray();

                String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                new upload(encodedImage).execute("");
                Log.d("Loaded File: ", file.toString());
            }

        }
    }


    public void setCanvasBackground(String string)
    {
        canvasView_text.setCanvasText(string);
        canvasView.setSaveFolder(string);
//        textToWrite.setTextSize(100);
//        textToWrite.setText(string);
    }

    public void setCanvasCharacter(String string)
    {
        this.mParam1 = string;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(boolean bool);
    }
}
class upload extends AsyncTask<String, Integer, String>
{
    String send_image;
    public upload(String encodedImage) {
        this.send_image = encodedImage;
    }

    @Override
    protected String doInBackground(String... params) {
        URL url = null;
        try {
            url = new URL("http://192.168.43.53:8000");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

//            Log.d("Response: ",httpURLConnection.getResponseMessage());
            try {
//                httpURLConnection.disconnect();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
                httpURLConnection.setChunkedStreamingMode(0);
//                httpURLConnection.connect();
                JSONObject json = new JSONObject();
                json.put("image",send_image);
                OutputStreamWriter out = new OutputStreamWriter(httpURLConnection.getOutputStream());
                out.write(json.toString());

                out.close();
                int httpResult = httpURLConnection.getResponseCode();
                if (httpResult == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(),"utf-8"));
                    String line = null;
                    while (((line) = bufferedReader.readLine()) != null) {
                        Log.d("Output",line);
                    }

                }
                else
                {
                    Log.d("Error","Returned an error");
                }
            }
            catch (IOException e)
            {
                Log.d("error in sending",e.toString());
            } catch (JSONException e) {
                Log.d("JSON error","check json");
                e.printStackTrace();
            } finally {
                httpURLConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
