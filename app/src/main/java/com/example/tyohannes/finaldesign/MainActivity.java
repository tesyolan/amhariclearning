package com.example.tyohannes.finaldesign;

import android.app.ActionBar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tyohannes.finaldesign.CharacterFragment.onFidelSelected;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, onFidelSelected, AdapterView.OnItemClickListener,
        LessonsFragment.OnFragmentInteractionListener, FidelFragment.OnFragmentInteractionListener,
        TabLayout.OnTabSelectedListener, QuizFragment.OnFragmentInteractionListener,
        WritingFragment.OnFragmentInteractionListener, AudioRecordFragment.OnFragmentInteractionListener {

    private int[] resource_id;
    private List<List<String>> fidel_list;
    private Lessons lessons;

    private InputStream in;
    private InputStream char_list;
    private ListView listView;
    private List<String> keys;

    private List<List<String>> lesson_list_iter;
    private List<List<String>> tobelearned_keys;

    private Map<String, List<String>> character_list;

    private List<String> simpleList;

    private int index;
    private int size;

    private LessonsFragment lessonsFragment;
    private CharacterFragment characterFragment;
    WritingFragment writingFragment;
    private QuizFragment quizFragment;
    private AudioRecordFragment audioRecordFragment;

    private FragmentTransaction transaction;

    private int selectedTab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        User user = (User) getIntent().getSerializableExtra("user");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView userName = (TextView) findViewById(R.id.nav_header_main_user_name);
        userName.setText(user.getName());

        ImageView imageView = (ImageView) findViewById(R.id.nav_header_main_user_avatar);
        resource_id = new int[]{R.drawable.boy_64, R.drawable.girl_64, R.drawable.male_64, R.drawable.girl_64};
        imageView.setImageResource(resource_id[user.getAvatar()]);


        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Learn"));
        tabLayout.addTab(tabLayout.newTab().setText("Quiz"));
        tabLayout.addTab(tabLayout.newTab().setText("Speech"));
        tabLayout.addTab(tabLayout.newTab().setText("Quiz"));
        tabLayout.addTab(tabLayout.newTab().setText("Writing"));
        tabLayout.addTab(tabLayout.newTab().setText("Quiz"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        tabLayout.addOnTabSelectedListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        listView = (ListView) findViewById(R.id.lessonlist_list);
        Log.d("it's empty", String.valueOf(listView == null));


        in = getResources().openRawResource(R.raw.lessons);
        char_list = getResources().openRawResource(R.raw.amharic);
        transaction= getSupportFragmentManager().beginTransaction();
        lessons = new Lessons(in, char_list);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        keys = null;
    }
    @Override
    public void onResume()
    {
        super.onResume();

//        lesson_list = lessons.create_entry(in);

        lesson_list_iter = lessons.returnLessonEntry();

        List<String> a = new ArrayList<>();

        for(List<String> i : lesson_list_iter )
        {
            String string ="";
            for(String k: i)
            {
                string = string + " ፤ " + k ;
            }
            a.add(string);
        }


        fidel_list = lessons.returnListCharList();
        character_list = lessons.returnCharListAll();

        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1,a));

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_menu_chart) {
            //TODO create a list of options to view the list of all.

            characterFragment = new CharacterFragment();
            characterFragment.setArguments(fidel_list);

            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_fragment_container,characterFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            return true;
        }
        if (id == R.id.action_setting) {
            //TODO create a list of options to view the list of all.
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.d("THis is called","called");
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFidelInteraction(String v) {
        transaction = getSupportFragmentManager().beginTransaction();
        FidelFragment fidelFragment = new FidelFragment();
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        transaction.replace(R.id.main_fragment_container,fidelFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        keys = lesson_list_iter.get(position);

        for(String key : keys)
        {
            Log.d("THis is called",key);
        }

        tobelearned_keys = new ArrayList<List<String>>();
        simpleList = new ArrayList<String>();

        tobelearned_keys = lessons.character_list(lesson_list_iter.get(position));
        for (List<String> s: tobelearned_keys)
        {
            for(int i=1; i < s.size(); i++)
            {
                simpleList.add(s.get(i));
            }
        }

        size = simpleList.size();

//        if (findViewById(R.id.main_fragment_container) != null)


        index =0;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onNextPressed() {

        index++;
        if(index<size)
        {
            lessonsFragment.setTextViewText(simpleList.get(index));
        }
        else
        {
            index=0;
            //TODO here we need to disable the button
        }

    }

    @Override
    public void onPrevPressed() {
        index--;
        if(index>0)
        {
            lessonsFragment.setTextViewText(simpleList.get(index));
        }
        else
        {
            index=0;
            //TODO here we need to disable the button
        }
    }
    @Override
    public void onCharPressed(String charPressed)
    {
        //Now go to the pressed and also set the index to that item.
//        Log.d("Button Pressed", charPressed);
        Log.d("Button ", charPressed.split(" ")[0] + " " + charPressed.split(" ")[1]);
        String[] indices = charPressed.split(" ");
        String view = character_list.get(indices[0]).get(Integer.parseInt(indices[1]));

        index = Integer.parseInt(indices[2]) * 7 + Integer.parseInt(indices[1]) - 1;
        lessonsFragment.setTextViewText(view);
    }

    @Override
    public void onFragmentInteraction() {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        selectedTab = tab.getPosition();

        //Now Before doing this; What if the place it was it was not selected.
        if(keys == null)
        {
            Toast.makeText(this,"Please Choose Lesson Entry",Toast.LENGTH_SHORT).show();
        }
        else
        {
            switch (selectedTab) {
                case 0:
                    Log.d("Tab: ","Lessons");
                    lessonsFragment = new LessonsFragment();
                    lessonsFragment.setArguments(tobelearned_keys);
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
                    transaction.replace(R.id.main_fragment_container, lessonsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case 1:
                    Log.d("Tab: ","Lessons Quiz");
                    quizFragment = new QuizFragment();
                    quizFragment.setArguments(tobelearned_keys);
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
                    transaction.replace(R.id.main_fragment_container, quizFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case 2:
                    audioRecordFragment = AudioRecordFragment.newInstance(simpleList.get(0),"");
//                    audioRecordFragment.setTextView(simpleList.get(0));
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    transaction.replace(R.id.main_fragment_container, audioRecordFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case 3:
                    Toast.makeText(this,"Sound Test not Yet Implemented",Toast.LENGTH_SHORT).show();
                    break;
                case 5:
                    Log.d("Tab","Writing Lesson");
                    //This lessons is as follow:
                    // It would bring the set of alphabet's from the system. and overlays it in the
                    // canvas. Then a person can go through the set of images in the system to view
                    // the different images.
                    writingFragment = new WritingFragment();
                    writingFragment.setCanvasCharacter(simpleList.get(0));
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
                    transaction.replace(R.id.main_fragment_container,writingFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case 6:
                    // This Quiz is going to be as follows:
                    // The Alphabet in question would be placed in a box in the bottom layer and the
                    // person would be tasked with writing the question. Then the person would be
                    // quized in the system. If the answer is writing then he may proceed onward.

                    // It must be cleaned up good and well. And I must make it elegant.
                    Toast.makeText(this,"Quiz for Writing",Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(this,"Feature Not Implemented",Toast.LENGTH_SHORT).show();
                    break;

            }
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.main_fragment_container, fragment).commit();
        }
        Log.d("Selected Tab", Integer.toString(selectedTab));
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    /**
     * Geniye This one is the interface that is implemented when the user
     * implments QuizFragment.OnFragmentInteractionListener
     * @param choice
     */
    @Override
    public void onChoice(int choice) {

        //Show some information and then on a user touch button go to next.
        boolean b = quizFragment.checkQuestion(choice);

        if(b)
        {
            quizFragment.setQuestion();
            Log.d("Answer is",Integer.toString(quizFragment.answer()));
        }

        //For simplicity i chose to add wrong to counter and then goes to fill the values.
    }

    @Override
    public void onFragmentInteraction(boolean bool) {
        //Fragment of Writing
        if (bool)
        {
            index++;
        }
        else
        {
            index--;
        }

        if(index < 0 || simpleList.size() == index)
            index = 0;


            Log.d("Setting","Writing Fragmet");
            writingFragment.setCanvasBackground(simpleList.get(index));


    }

    @Override
    public void OnFragmentInteractionListenerAudio(boolean bool) {
        if (bool)
        {
            index++;
        }
        else
        {
            index--;
        }

        if(index < 0 || simpleList.size() == index)
            index = 0;

            Log.d("setting","Audio Fragmetn");
            audioRecordFragment.setTextView(simpleList.get(index));

    }
}
