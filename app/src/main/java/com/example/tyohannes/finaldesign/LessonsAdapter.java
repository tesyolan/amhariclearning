package com.example.tyohannes.finaldesign;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tyohannes on 1/10/17.
 */

public class LessonsAdapter extends ArrayAdapter<String> {
    private int resource;
    String response;
    Context context;
    private List<String> items;
    public LessonsAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.items = items;
        this.resource=resource;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LinearLayout contactsView;
        String contact = items.get(position);
        if(convertView==null) {
            contactsView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater)getContext().getSystemService(inflater);
            vi.inflate(resource, contactsView, true);
        } else {
            contactsView = (LinearLayout) convertView;
        }
        TextView lessonName =(TextView)contactsView.findViewById(R.id.lesson_name_text);
//        lessonName.setTypeface(null, Typeface.BOLD);
//        lessonName.setTextSize(TypedValue.COMPLEX_UNIT_PX,18);

        lessonName.setText(contact);

        return contactsView;
    }
}
