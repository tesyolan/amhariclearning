package com.example.tyohannes.finaldesign;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tyohannes on 1/8/17.
 */

public class UserDAO extends SQLiteOpenHelper {
    public UserDAO(Context context) {
        super(context, "StudentID", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE Table users (id INTEGER PRIMARY_KEY, name TEXT NOT NULL, avatar INTEGER, progress INTEGER)";
        db.execSQL(sql);
        sql = "CREATE Table last_users (id INTEGER PRIMARY_KEY, name TEXT NOT NULL)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insert(User user) {
        ContentValues data = new ContentValues();
        data.put("name", user.getName());
        data.put("avatar", user.getAvatar());

        SQLiteDatabase database = getWritableDatabase();
        database.insert("users",null,data);
    }

    public List<User> listAll() {
        List<User> users = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM users; ", null);
        while(cursor.moveToNext())
        {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            int avatar = cursor.getInt(cursor.getColumnIndex("avatar"));
            int progress = cursor.getInt(cursor.getColumnIndex("progress"));
            int id = cursor.getInt(cursor.getColumnIndex("id"));

            User user = new User(name, id, avatar, progress);
            users.add(user);
        }
        cursor.close();
        return users;
    }
    public void lastUser(User user)
    {
        ContentValues data = new ContentValues();
        data.put("name", user.getName());

        SQLiteDatabase database = getWritableDatabase();
        database.insert("last_users",null,data);
    }
    public User getLastUser() {
        User lastUser = null;
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM last_users; ", null);
        if(cursor.moveToLast())
        {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            int avatar = cursor.getInt(cursor.getColumnIndex("avatar"));
            int progress = cursor.getInt(cursor.getColumnIndex("progress"));
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            lastUser = new User(name, id, avatar, progress);
        }
        cursor.close();
        return lastUser;
    }
    public void deleteTables() {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("delete from users");
    }
}
