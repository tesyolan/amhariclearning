package com.example.tyohannes.finaldesign;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.List;

public class UserLoginActivity extends AppCompatActivity {
    private Button play_Button;
    private Spinner spinner;
    private ImageView imageView;
    private int[] resource_id;
    private User selected_user;
    private UserDAO dao;
    private List<User> users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        Stetho.initializeWithDefaults(this);

        resource_id = new int[]{R.drawable.boy_64, R.drawable.girl_64, R.drawable.male_64, R.drawable.female_64};
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        dao = new UserDAO(this);



        play_Button = (Button) findViewById(R.id.activity_user_login_play);
        imageView = (ImageView) findViewById(R.id.activity_user_login_avatar);
        spinner = (Spinner) findViewById(R.id.activity_user_login_list);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.activity_user_login_add_user);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLoginActivity.this, NewUserActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        users = dao.listAll();
        if (users.size() == 0)
        {
            imageView.setImageResource(R.drawable.unknown_64);
            play_Button.setEnabled(false);
        }
        else
        {
            imageView.setImageResource(resource_id[3]);
//            User user = dao.getLastUser();

            play_Button.setEnabled(true);
        }
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_spinner_dropdown_item, users);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_user = (User) parent.getItemAtPosition(position);

                imageView.setImageResource(resource_id[selected_user.getAvatar()]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                imageView.setImageResource(R.drawable.unknown_64);
                //disable buttons if desired.
            }

        });
        if (play_Button.isEnabled())
        {
            Log.i("Reaches Button Enabled","i see");
            play_Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UserLoginActivity.this, MainActivity.class);
                    intent.putExtra("user", selected_user);
                    startActivity(intent);
                }
            });
        }
        else
        {
            Log.i("Reaches Button Enabled","i dont see");
            Toast.makeText(UserLoginActivity.this, "ሰው ፍጠር",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dao.close();
    }
}
